
var LocalStrategy   = require('passport-local').Strategy;
var passport = require('passport');

var superUser = { email: 'tracker@fresh-ads.com', id:'2112', pwd: 'Tracker2112' }

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
        done(null, superUser);
});

passport.use( new LocalStrategy(
	function(username, password, done) { // callback with email and password from our form

			if( superUser.pwd != password )
				return done(null, false,{ message: 'Incorrect password.' })

			return done(null, superUser);


	})
);

module.exports = {
	ensureLoggedIn: function(options) {

		if (typeof options == 'string') {
			options = { redirectTo: options }
		}
		options = options || {};

		var url = options.redirectTo || '/login';
		var setReturnTo = (options.setReturnTo === undefined) ? true : options.setReturnTo;

		return function(req, res, next) {

			if (!req.isAuthenticated || !req.isAuthenticated()) {
			  if (setReturnTo && req.session) {
			    req.session.returnTo = req.originalUrl || req.url;
			  }
			  return res.redirect(url);
			}
			next();
		}
	}

}

