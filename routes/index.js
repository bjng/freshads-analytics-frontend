var express = require('express');
var router = express.Router();
var ensureLoggedIn = require('../authenticate/authenticate').ensureLoggedIn;
var passport = require('passport');
var request = require( 'request' );



/* GET Login page. */
router.get('/login', function(req, res) {
	res.render('login', {title: 'Login'});
});


router.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

router.post('/login', passport.authenticate('local', { successReturnToOrRedirect: '/', failureRedirect: '/login' } ));


/* GET home page. */
router.get('/:id?/:start?/:end?',ensureLoggedIn(), function(req, res) {
  
	var id = req.params.id || '',
		start = req.params.start,
		end = req.params.end,
		today = new Date(),
		endDate = end? new Date( end + ' UTC') : today,
		startDate = start ? new Date( start + ' UTC') : new Date( endDate.getTime() );
	
	if(endDate > today){
		endDate = today;
	}
	if(!start || startDate > endDate ){
		startDate.setDate( endDate.getDate()-7 );
	}

	start = startDate.toISOString().substr(0,10);
	end = endDate.toISOString().substr(0,10);
	
	var newPath = '/' + [id,start,end].join('/');


	
	var start = startDate.toISOString().substr(0,10),
		end = endDate.toISOString().substr(0,10),
		newPath = '/' + [id,start,end].join('/');

	if(!id){ //
		return res.render('index', { uid:req.params.id, data:null, apiUri:'', start:start, end:end});
	}
	else if( req.path != newPath ){ //start end date has been changed due to invalid dates
		return res.redirect( newPath );
	}


	//otherwise get analytics json data from API
	var apiUri = [id,start,end].join('/'),//also used in template
		jsonUrl =  ['http://freshadsapi.wsm-qi.com/daily',apiUri].join('/') + '?reduce=1';
	
	request({
		url: jsonUrl,
		json: true
	}, function (err, response, data) {
		if( err ){
			return res.send( 404, err.toString() );
		}
		else if (response.statusCode === 200) {
		    return res.render('index', { uid:req.params.id, data:data, apiUri: apiUri, start:start, end:end});
		}

		return res.send( 404 );
		
	});

	//console.log(start,end);

	//res.render('index', { uid:req.params.id, data:table, start:start, end:end /*title: 'url = ../' + id + '/'+ startDate.toUTCString() + '/'+ endDate.toUTCString()*/});


});


module.exports = router;

