#Freshads - Analytics

Project for [Daily Mail UK](http://www.dailymail.co.uk/)

Daily Mail publishes a mobile version of their daily newspaper.
They needed a toolset that could deliver quickly Ads with some standard components like Image Galleries, Videos, Forms and 3D Turntables, that would be embedded into the electronic issues. It should be so easy to use that even an intern could create an ad very fast and the platform should also tracks reliable views and Interactions.

The project consisted of following parts:

1. A sophisticated [Ad Builder](https://bitbucket.org/bjng/freshads-builder) based on HTML5 Canvas with a Python backend
2. Analytics Tracker Backend [Node.js Source](https://bitbucket.org/bjng/freshads-analytics/src) in particular the [Analytics Storage](https://bitbucket.org/bjng/freshads-analytics/src/9fa52a6f8a9cd90825c32f25f5ee479fd8cf09db/analytics.js?at=master&fileviewer=file-view-default)
3. Analytics clients script, [source](https://bitbucket.org/bjng/freshads-analytics/src/9fa52a6f8a9cd90825c32f25f5ee479fd8cf09db/client/src/fa.js?at=master&fileviewer=file-view-default)
4. A little in-house admin interface to view the Stats in Express.js [Analytics Admin](https://bitbucket.org/bjng/freshads-analytics-frontend)

---


This repo contains Part 4.


#Technologies

Node.js, Express.js, Mongo, Jade


This is a pretty standard Express.js app with rudimetary authentification for in-house Use only.