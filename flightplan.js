// flightplan.js
var Flightplan = require('flightplan');



var plan = new Flightplan();



// configuration
plan.briefing({
  debug: false,
  destinations: {
  	'development': {
  		name: 'freshadswebapp',
		host: 'wsm-qi.com',
		instances: 1,
		username: 'root',
		path: '/home/analytics.fresh-ads.com',
		git: 'git@git.wsm-qi.com:root/freshads-analytics-webapp.git',
		agent: process.env.SSH_AUTH_SOCK
    },
  }
});

// run commands on localhost
plan.local(function(local) {
  //local.log('Run build');
  //local.exec('gulp');
});

// run commands on remote hosts (destinations)
plan.remote(function(remote) {

 console.log( remote.target.folder )
});


plan.remote('install', function(remote) {
	remote.rm( '-rf ' + remote.target.path, {silent: true});
	remote.exec( 'mkdir ' + remote.target.path, {silent: true});
	remote.with( 'cd ' + remote.target.path, function(){
 		console.log( remote.flight  );
 		remote.exec( 'pm2 delete ' + remote.target.name, {failsafe:true} );
 		remote.git('clone ' + remote.target.git + ' .');
 		remote.exec('npm install');
 		remote.exec( 'pm2 start ' +  remote.target.path + '/' + 'app.js --name ' + '"' + remote.target.name + '"' +  ' -i ' + remote.target.instances)
	});
});

plan.remote('update', function(remote) {
	remote.with( 'cd ' + remote.target.path, function(){
 		remote.git('pull ');
 		remote.exec('npm install');
 		remote.exec('pm2 restart ' + remote.target.name );	
	});
});


// executed if flightplan succeeded
plan.success(function() { /* ... */ });

// executed if flightplan failed
plan.disaster(function() { /* ... */ });

// always executed after flightplan finished
plan.debriefing(function() { /* ... */ });